// Стандартные библиотеки C++
#include "mpi.h"    // Для MPI функций
#include <cmath>    // Для работы с мат. функциями
#include <iostream> // Для вывода данных на экран
#include <iomanip>  // Для форматированного вывода
#include <stdlib.h> // Для работы с atof()

// Переопределение A[x * nodes + y] ~ A[x][y]
#define oldProcT(x, y) oldProcT[(x) * nodes + (y)]
#define newProcT(x, y) newProcT[(x) * nodes + (y)]
#define    array(x, y)    array[(x) * nodes + (y)]

const int layers = 100;       //!< Число слоёв сетки по пространству
const int nodes = layers + 1; //!< Число узлов сетки по одной стороне
const double size = 0.01;     //!< Длина квадратной сетки

const double maxCND = 0.000006161788205; //!< Максимальный коэффициент теплопроводности в CFL условии

double calcTime = 5.0;                //!< Общее время расчёта
double currentTime = 0.0;             //!< Текущее время расчёта
double dx = size / layers;            //!< Шаг по координате
double dt = 0.225 * dx * dx / maxCND; //!< Шаг по времени

//! Матрицы, которые содержат разделённые части исходной сетки
double*  oldProcT;
double*  newProcT;
double* temporary;

int xCoord = 0; //!< Координата x для каждого процесса

double getC(double temp);      //!< Удельная теплоёмкость
double getRO(double temp);     //!< Плотность
double getLAMBDA(double temp); //!< Коэффициент теплопроводности

double getInitialTemp(double x, double y);          // Получить значение температуры при t = 0 в точке (x, y)
double getBorderTemp(double x, double y, double t); // Получить значение температуры на границах в точке (x, y)
void   procTimeStep(int x, int y);                  // Получить значение температуры на сетки при очередном временном шаге

int main(int argc, char* argv[])
{
    // Инициализируем параллельную часть программы
    MPI_Init(&argc, &argv);

    // Получаем аргументы командной строки
    if (argc == 2)
    {
        calcTime = atof(argv[1]);
        if (calcTime < 0)
        {
            printf("Error with initial parameter.\n");
            return -1;
        };
    }
    else if ((argc != 1) && (argc != 2))
    {
        printf("There are too many parameters.\n");
        return -2;
    };

    // Определяем число процессов
    int numProcess;
    MPI_Comm_size(MPI_COMM_WORLD, &numProcess);

    // Определяем номер процесса
    int idProcess;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProcess);

    // Определяем, сколько строк необходимо выделить на каждый процесс
    int rowPerProc = nodes / numProcess;
    rowPerProc  = ((nodes - rowPerProc * numProcess) > idProcess) ? rowPerProc + 1 : rowPerProc;

    // Вычисляем значение переменной xCoord, используя данные о количестве строк предыдущих процессов
    if ((nodes % numProcess) >= idProcess)
        xCoord = (nodes / numProcess + 1) * idProcess;
    else
        xCoord = (nodes / numProcess + 1) * idProcess - (idProcess - nodes % numProcess);

    // Матрицы, которые содержат разделённые части исходной сетки
     oldProcT = new double[(rowPerProc + 2) * nodes * sizeof(double)];
     newProcT = new double[(rowPerProc + 2) * nodes * sizeof(double)];

    // Отсчёт времени работы программы
    double beginTime = MPI_Wtime();

    //-------------------------------------------------------------

    // Ввод начальных данных

    // Вычисляем значение температуры на границах слева и справа
    for (int x = 1; x < rowPerProc + 1; x++)
    {
      oldProcT(x,         0) = getBorderTemp((xCoord + x - 1) * dx,  0.0, 0.0);
      oldProcT(x, nodes - 1) = getBorderTemp((xCoord + x - 1) * dx, size, 0.0);
    };
    // Вычисляем значение температуры на границах сверху и снизу
    if (idProcess == 0)
        for (int y = 0; y < nodes; y++)
          oldProcT(1, y) = getBorderTemp(0.0, y * dx, 0.0);
    if (idProcess == (numProcess - 1))
        for (int y = 0; y < nodes; y++)
          oldProcT(rowPerProc, y) = getBorderTemp(size, y * dx, 0.0);
    // Вычисляем значение температуры при t = 0
    for (int x = 1; x < rowPerProc + 1; x++)
        for (int y = 1; y < nodes - 1; y++)
            oldProcT(x, y) = getInitialTemp((xCoord + x - 1) * dx, y * dx);

    while((currentTime <= calcTime) && (calcTime != 0.0))
    {
      // Обмениваемся данными между процессами
      int prevProcess = (idProcess ==                0) ? MPI_PROC_NULL : idProcess - 1;
      int nextProcess = (idProcess == (numProcess - 1)) ? MPI_PROC_NULL : idProcess + 1;
      MPI_Sendrecv(oldProcT + nodes * rowPerProc, nodes, MPI_DOUBLE, nextProcess, 1, oldProcT, nodes, MPI_DOUBLE, prevProcess, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Sendrecv(oldProcT + nodes, nodes, MPI_DOUBLE, prevProcess, 1, oldProcT + nodes * (rowPerProc + 1), nodes, MPI_DOUBLE, nextProcess, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // Вычисляем значение температуры на границах слева и справа
      for (int x = 1; x < rowPerProc + 1; x++)
      {
        newProcT(x,         0) = getBorderTemp((xCoord + x - 1) * dx,  0.0, currentTime + dt);
        newProcT(x, nodes - 1) = getBorderTemp((xCoord + x - 1) * dx, size, currentTime + dt);
      };

     // Вычисляем значение температуры внутри сетки
     for (int x = 1; x < rowPerProc + 1; x++)
        for (int y = 1; y < nodes - 1; y++)
            procTimeStep(x, y);

      // Вычисляем значение температуры на границах сверху и снизу
      if (idProcess == 0)
          for (int y = 0; y < nodes; y++)
            newProcT(1, y) = getBorderTemp(0.0, y * dx, currentTime + dt);
      if (idProcess == (numProcess - 1))
          for (int y = 0; y < nodes; y++)
            newProcT(rowPerProc, y) = getBorderTemp(size, y * dx, currentTime + dt);

      // Меняем местами значения сетки с предыдущей на следующую итерацию
      temporary =  oldProcT;
       oldProcT =  newProcT;
       newProcT = temporary;

      // Переходим на следующий шаг по времени
      currentTime += dt;
    };

    //-------------------------------------------------------------

    // Конец отсчёта времени работы программы
    double endTime = MPI_Wtime();

    // Выводим время работы программы
    if (idProcess == 0)
        std::cout << "Time: " << endTime - beginTime << std::endl;
    delete[] oldProcT;
    delete[] newProcT;

    // Завершаем работу параллельной части программы
    MPI_Finalize();

	return 0;
};

double getC(double temp)
{
	return 1. / (2.25E-3 - 6.08E-10 * temp * temp);
};
double getRO(double temp)
{
	return 7860 + 41500 / temp;
};
double getLAMBDA(double temp)
{
	return 1.45 + 2.3E-2 * temp - 2E-6 * temp * temp;
};
double getInitialTemp(double x, double y)
{
    x = x / size;
    y = y / size;

	return 100 * (1 + y) * sqrt(25 - 16 * x * x);
};
double getBorderTemp(double x, double y, double t)
{
    x = x / size;
    y = y / size;
    t = t / calcTime;

	double temp;
    if (x ==  0.0 / size) temp = 500 * (1 + y) + 200 * t * cos(2 * M_PI * y);
    if (x == size / size) temp = 300 * (1 + y) + t * (250 * sin(M_PI * y / 2) + 350);
    if (y ==  0.0 / size) temp = 100 * sqrt(25 - 16 * x * x) + t * (150 * x + 200);
    if (y == size / size) temp = 200 * sqrt(25 - 16 * x * x) + t * (200 * sin(M_PI * x / 2) + 200 * (1 + x));
    return temp;
};
void procTimeStep(int x, int y)
{
    double leftCND, rightCND;
    double   xSMND,    ySMND;

    // Расчёт плотности теплового потока по оси OX
    rightCND = getLAMBDA((oldProcT(x + 1, y) + oldProcT(x, y)) / 2);
    leftCND  = getLAMBDA((oldProcT(x - 1, y) + oldProcT(x, y)) / 2);
    xSMND = (rightCND * (oldProcT(x + 1, y) - oldProcT(x, y)) - leftCND * (oldProcT(x, y) - oldProcT(x - 1, y))) / dx / dx;

    // Расчёт плотности теплового потока по оси OY
    rightCND = getLAMBDA((oldProcT(x, y + 1) + oldProcT(x, y)) / 2);
    leftCND  = getLAMBDA((oldProcT(x, y - 1) + oldProcT(x, y)) / 2);
    ySMND = (rightCND * (oldProcT(x, y + 1) - oldProcT(x, y)) - leftCND * (oldProcT(x, y) - oldProcT(x, y - 1))) / dx / dx;

    // Расчёт температуры на следующем временном шаге
    newProcT(x, y) = oldProcT(x, y) + dt * (xSMND + ySMND) / getC(oldProcT(x, y)) / getRO(oldProcT(x, y));
};
