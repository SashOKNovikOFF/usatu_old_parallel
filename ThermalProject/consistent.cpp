// Стандартные библиотеки C++
#include <cmath>    // Для работы с мат. функциями
#include <string>   // Для работы со строками
#include <fstream>  // Для вывода данных в файл
#include <iostream> // Для вывода данных на экран
#include <iomanip>  // Для форматированного вывода
#include <stdlib.h> // Для работы с atof()

// Переопределение A[x * nodes + y] ~ A[x][y]
#define oldTemp(x, y) oldTemp[(x) * nodes + (y)]
#define newTemp(x, y) newTemp[(x) * nodes + (y)]
#define   array(x, y)   array[(x) * nodes + (y)]

const int layers = 100;       //!< Число слоёв сетки по пространству
const int nodes = layers + 1; //!< Число узлов сетки по одной стороне
const double size = 0.01;     //!< Длина квадратной сетки

const double maxCND = 0.000006161788205; //!< Максимальный коэффициент теплопроводности в CFL условии

double calcTime = 5.0;                //!< Общее время расчёта
double dx = size / layers;            //!< Шаг по координате
double dt = 0.225 * dx * dx / maxCND; //!< Шаг по времени
double currentTime = 0.0;             //!< Текущее время расчёта

double* oldTemp; //!< Температура на предыдущем временном слое
double* newTemp; //!< Температура на текущем временном слое

std::string outputFile = "outputFile.txt"; //!< Название выходного файла

double getC(double temp);      //!< Удельная теплоёмкость
double getRO(double temp);     //!< Плотность
double getLAMBDA(double temp); //!< Коэффициент теплопроводности

double getInitialTemp(double x, double y);          // Получить значение температуры при t = 0 в точке (x, y)
double getBorderTemp(double x, double y, double t); // Получить значение температуры на границах в точке (x, y)
void timeStep();                                    // Получить значение температуры на сетки при очередном временном шаге

void printMatrix(double* array, int nodes);                      // Вывод матрицы на экран
void saveData(std::string outputFile, double* array, int nodes); // Сохранить данные матрицы в файл

int main(int argc, char* argv[])
{
    // Получаем аргументы командной строки
    if (argc == 3)
    {
        calcTime = atof(argv[1]);
        outputFile.assign(argv[2]);
        if (calcTime < 0)
        {
            printf("Error with initial parameter.\n");
            return -1;
        };
    }
    else if ((argc != 1) && (argc != 3))
    {
        printf("There are too many parameters.\n");
        return -2;
    };

	// Инициализируем исходные данные
	oldTemp = new double[nodes * nodes];
	newTemp = new double[nodes * nodes];

	// Вычисляем значение температуры на границах
	for (int x = 0; x < nodes; x++)
	{
        oldTemp(        0, x) = getBorderTemp(   0.0, x * dx, 0.0);
        oldTemp(nodes - 1, x) = getBorderTemp(  size, x * dx, 0.0);
        oldTemp(x,         0) = getBorderTemp(x * dx,    0.0, 0.0);
        oldTemp(x, nodes - 1) = getBorderTemp(x * dx,   size, 0.0);
	};

	// Вычисляем значение температуры при t = 0
    for (int x = 1; x < nodes - 1; x++)
        for (int y = 1; y < nodes - 1; y++)
            oldTemp(x, y) = getInitialTemp(x * dx, y * dx);

    while((currentTime <= calcTime) && (calcTime != 0.0))
    {
        timeStep();
        std::cout << "Result: " << currentTime / calcTime * 100 << "%; dt = " << dt << std::endl;
    };

    // DEBUG: Вывод матрицы на экран
    //printMatrix(newTemp, nodes);

	// Сохраняем результаты в файл
    saveData(outputFile, newTemp, nodes);

	return 0;
};

double getC(double temp)
{
	return 1. / (2.25E-3 - 6.08E-10 * temp * temp);
};
double getRO(double temp)
{
	return 7860 + 41500 / temp;
};
double getLAMBDA(double temp)
{
	return 1.45 + 2.3E-2 * temp - 2E-6 * temp * temp;
};
double getInitialTemp(double x, double y)
{
    x = x / size;
    y = y / size;

	return 100 * (1 + y) * sqrt(25 - 16 * x * x);
};
double getBorderTemp(double x, double y, double t)
{
    x = x / size;
    y = y / size;
    t = t / calcTime;

	double temp;
    if (x ==  0.0 / size) temp = 500 * (1 + y) + 200 * t * cos(2 * M_PI * y);
    if (x == size / size) temp = 300 * (1 + y) + t * (250 * sin(M_PI * y / 2) + 350);
    if (y ==  0.0 / size) temp = 100 * sqrt(25 - 16 * x * x) + t * (150 * x + 200);
    if (y == size / size) temp = 200 * sqrt(25 - 16 * x * x) + t * (200 * sin(M_PI * x / 2) + 200 * (1 + x));
    return temp;
};
void timeStep()
{
    double leftCND, rightCND;
    double   xSMND,    ySMND;

    // Вычисляем значение температуры на границах
    for (int x = 0; x < nodes; x++)
    {
        newTemp(        0, x) = getBorderTemp(   0.0, x * dx, currentTime + dt);
        newTemp(nodes - 1, x) = getBorderTemp(  size, x * dx, currentTime + dt);
        newTemp(x,         0) = getBorderTemp(x * dx,    0.0, currentTime + dt);
        newTemp(x, nodes - 1) = getBorderTemp(x * dx,   size, currentTime + dt);
    };

    // Вычисляем значение температуры внутри сетки
    for (int x = 1; x < nodes - 1; x++)
        for (int y = 1; y < nodes - 1; y++)
        {
            // Расчёт плотности теплового потока по оси OX
            rightCND = getLAMBDA((oldTemp(x + 1, y) + oldTemp(x, y)) / 2);
            leftCND  = getLAMBDA((oldTemp(x - 1, y) + oldTemp(x, y)) / 2);
            xSMND = (rightCND * (oldTemp(x + 1, y) - oldTemp(x, y)) - leftCND * (oldTemp(x, y) - oldTemp(x - 1, y))) / dx / dx;

            // Расчёт плотности теплового потока по оси OY
            rightCND = getLAMBDA((oldTemp(x, y + 1) + oldTemp(x, y)) / 2);
            leftCND  = getLAMBDA((oldTemp(x, y - 1) + oldTemp(x, y)) / 2);
            ySMND = (rightCND * (oldTemp(x, y + 1) - oldTemp(x, y)) - leftCND * (oldTemp(x, y) - oldTemp(x, y - 1))) / dx / dx;
            
            // Расчёт температуры на следующем временном шаге
            newTemp(x, y) = oldTemp(x, y) + dt * (xSMND + ySMND) / getC(oldTemp(x, y)) / getRO(oldTemp(x, y));
        };

    currentTime += dt; // Увеличиваем текущее время расчёта

    double* temporary;
    temporary = oldTemp;
    oldTemp   = newTemp;
    newTemp   = temporary;
};
void printMatrix(double* array, int nodes)
{
    for (int x = 0; x < nodes; x++)
    {
        for (int y = 0; y < nodes; y++)
            std::cout << array(x, y) << "\t";
        std::cout << std::endl;
    };
};
void saveData(std::string outputFile, double* array, int nodes)
{
  std::fstream resultFile(outputFile.c_str(), std::ifstream::out);
  resultFile << std::fixed << std::setprecision(8);

  for (int x = 0; x < nodes; x++)
  {
    for (int y = 0; y < nodes; y++)
      resultFile << x * dx << "\t" << y * dx << "\t" << array(x, y) << std::endl;
    resultFile << std::endl;
  };

  resultFile.close();
};
