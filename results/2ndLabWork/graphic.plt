reset

### For .jpeg file creation
set terminal jpeg size 1024, 768
set output "N-Q-Time-S-E.jpg"

set xlabel "Size of matrix N"
set ylabel "Speed up S"

plot "N-Q-Time-S-E.txt" using 1:5 with lines lc 24 dt 3 notitle