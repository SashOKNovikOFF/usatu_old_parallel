// C standard includes
#include "mpi.h"    // for MPI functions
#include <stdio.h>  // for printf()
#include <stdlib.h> // for atoi()
#include <math.h>   // for math functions

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    long long int length = 100;
    long double series = 0.0;
    double startTime, endTime;

    if (argc == 2)
    {
	length = atoll(argv[1]);
	if (length <= 0)
	{
	    printf("Error with initial parameter.\n");
	    return -1;
	};
    }
    else if (argc >= 2)
    {
	printf("There are too many parameters.\n");
	return -2;
    };

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int idProcess;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProcess);

    startTime = MPI_Wtime();

    for (long long int n = 2 * idProcess + 1; n <= length; n += 2 * size)
	series -= (n + 1) / ((n + 1) * sqrt(n + 1) - 1);

    for (long long int n = 2 * idProcess + 2; n <= length; n += 2 * size)
        series += (n + 1) / ((n + 1) * sqrt(n + 1) - 1);

    if (idProcess == 0)
    {
	MPI_Status status;
	long double temp;

	for (int id = 1; id < size; id++)
	{
	    MPI_Recv(&temp, 1, MPI_LONG_DOUBLE, id, 1, MPI_COMM_WORLD, &status);
	    series += temp;
	};

        endTime = MPI_Wtime();

	printf("Sum of series: %10.9Lf, time: %10.9f \n", series, endTime - startTime);
    }
    else
	MPI_Send(&series, 1, MPI_LONG_DOUBLE, 0, 1, MPI_COMM_WORLD);

    MPI_Finalize();

    return 0;
}
