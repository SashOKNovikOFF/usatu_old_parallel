// C standard includes
#include <stdio.h>
#include <math.h>
#include <time.h>

int main()
{
    long long int length = 22500000000;
    long double series = 0.0;
    
    clock_t beginTime;
    clock_t endTime;
    float resultTime;

    beginTime = clock();
    
    for (long long int n = 1; n <= length; n += 2)
        series -= (n + 1) / ((n + 1) * sqrt(n + 1) - 1);
    for (long long int n = 2; n <= length; n += 2)
        series += (n + 1) / ((n + 1) * sqrt(n + 1) - 1);

    endTime = clock();
    resultTime = (float)(endTime - beginTime) / CLOCKS_PER_SEC;
    
    printf("Sum of series: %10.9f \n", series);
    printf("Time of running: %10.9f \n", resultTime);

    return 0;
}