// C standard includes
#include <stdio.h>
#include <math.h>
#include <omp.h>

int main()
{
    long long int length = 22500000000;
    long double series = 0.0;
    
	float beginTime;
	float endTime;
    float resultTime;

    beginTime = omp_get_wtime();

	#pragma omp parallel reduction(+:series)
	{
		#pragma omp for
		for (long long int n = 1; n <= length; n += 2)
			series -= (n + 1) / ((n + 1) * sqrt(n + 1) - 1);
	}

	#pragma omp parallel reduction(+:series)
	{
		#pragma omp for
		for (long long int n = 2; n <= length; n += 2)
			series += (n + 1) / ((n + 1) * sqrt(n + 1) - 1);
	}

    endTime = omp_get_wtime();
    resultTime = endTime - beginTime;
    
    printf("Sum of series: %10.9f \n", series);
    printf("Time of running: %10.9f \n", resultTime);

    return 0;
}