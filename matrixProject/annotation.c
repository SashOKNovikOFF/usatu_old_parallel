// C standard includes
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "advisor-annotate.h"

void printMatrix(double** matrix, int row, int column);
double getMatrixNorm(double** matrix, int row, int column);
int main()
{
	int length = 500;
	double** matrixA;
	double** matrixB;
	double** matrixC;
    
	//!< ��������� ������ �� �������� ��� ������
	matrixA = (double**)malloc(length * sizeof(double*));
	matrixB = (double**)malloc(length * sizeof(double*));
	matrixC = (double**)malloc(length * sizeof(double*));
	for (int i = 0; i < length; i++)
	{
		matrixA[i] = (double*)malloc(length * sizeof(double));
		matrixB[i] = (double*)malloc(length * sizeof(double));
		matrixC[i] = (double*)malloc(length * sizeof(double));
	};

	//!< ���������� ������ ���������� �������
	srand((unsigned int)time(NULL));
	for (int i = 0; i < length; i++)
		for (int j = 0; j < length; j++)
		{
			matrixA[i][j] = (double)rand() / RAND_MAX - 0.5;
			matrixB[i][j] = (double)rand() / RAND_MAX - 0.5;
			matrixC[i][j] = 0.0;
		};
	
	clock_t beginTime;
	clock_t endTime;
	float resultTime;

	//!< ���� ����� �������� ��������� �������
	long long int repeatNumber = 1;
	//printf("Enter the number of repeats [Q]: ");
	//scanf_s("%lld", &repeatNumber);

	beginTime = clock();
	
	for (long long int repeat = 0; repeat < repeatNumber; repeat++)
	{
		for (long long int i = 0; i < length; i++)
			for (long long int j = 0; j < length; j++)
				matrixC[i][j] = 0;

		ANNOTATE_SITE_BEGIN(MAINLOOP);
		for (long long int i = 0; i < length; i++)
		{
			ANNOTATE_ITERATION_TASK(MAINFOR);
			for (long long int k = 0; k < length; k++)
			{
				for (long long int j = 0; j < length; j++)
				{
					matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
				};
			};
		};
		ANNOTATE_SITE_END();
	};
	
	endTime = clock();
	resultTime = (float)(endTime - beginTime) / CLOCKS_PER_SEC;

	printf("Time of running: %f\n", resultTime);
	printf("Matrix norm: %f\n", getMatrixNorm(matrixC, length, length));

	for (int i = 0; i < length; i++)
	{
		free(matrixA[i]);
		free(matrixB[i]);
		free(matrixC[i]);
	};
	free(matrixA);
	free(matrixB);
	free(matrixC);
    

    return 0;
}

void printMatrix(double** matrix, int row, int column)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
			printf("%f\t", matrix[i][j]);
		printf("\n");
	};
};
double getMatrixNorm(double** matrix, int row, int column)
{
	double sum = 0;
	for (long long int i = 0; i < row; i++)
		for (long long int j = 0; j < column; j++)
			sum += matrix[i][j] * matrix[i][j];
	
	return sqrt(sum);
};