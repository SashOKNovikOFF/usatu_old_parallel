// Стандартные C-библиотеки
#include "mpi.h"    // Для MPI функции
#include <stdio.h>  // Для функции printf()
#include <stdlib.h> // Для функций atoi(), srand(), INT_MAX
#include <time.h>   // Для функции time()
#include <math.h>   // Для математических функций

int initializeLength(int argc, char* argv[], int* column);        //!< Ввести пользовательское число столбцов матрицы
void randomizeMatrixValues(double** matrix, int row, int column); //!< Заполнить матрицу случайными числами
void printMatrix(double** matrix, int row, int column, int id);   //!< Напечатать матрицу на экране
double getMatrixNorm(double** matrix, int row, int column);       //!< Получить значение нормы матрицы

int main(int argc, char* argv[])
{
    // Инициализируем параллельную часть программы
    MPI_Init(&argc, &argv);
    
    // Число строк квадратной матрицы B
    int lengthR = 5;

    // Вводим пользовательское число строк матрицы B
    if (initializeLength(argc, argv, &lengthR) != 0)
        return -1;

    // Число строк матрицы A
    int lengthL = 10 * lengthR;

    // Матрицы A, B, C
    double* matrixA;
    double* matrixB;
    double* matrixC;

    // Выделяем память на создание матрицы B
    matrixB = (double*)malloc(lengthR * lengthR * sizeof(double));

    // Определяем число процессов
    int numProcess;
    MPI_Comm_size(MPI_COMM_WORLD, &numProcess);

    // Определяем номер процесса
    int idProcess;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProcess);

    // Определяем, сколько строк необходимо выделить на каждый процесс
    int rowPerProc = lengthL / numProcess;
    rowPerProc = ((lengthL - rowPerProc * numProcess) > idProcess) ? rowPerProc + 1 : rowPerProc;

    // Временные массивы для функции MPI_Scatterv
    int* displs;
    int* rcounts;

    // Разделяем матрицу A на части, каждая из которых будет обрабатываться отдельным процессом
    double* matrixT = (double*)malloc(rowPerProc * lengthR * sizeof(double));

    // Результат перемножения матрицы T и B для каждого отдельного процесса
    double* matrixR = (double*)calloc(rowPerProc * lengthR, sizeof(double));

    // Инициализируем данные первым процессом
    if (idProcess == 0)
    {
        // Выделяем память на создание матриц A, C
        matrixA = (double*)malloc(lengthL * lengthR * sizeof(double));
        matrixC = (double*)malloc(lengthL * lengthR * sizeof(double));

        // Заполняем матрицы A, B случайными числами
        srand(0);
        randomizeMatrixValues(&matrixA, lengthL, lengthR);
        randomizeMatrixValues(&matrixB, lengthR, lengthR);

        // Выделяем память под временные массивы для функции MPI_Scatterv
        displs  = (int*)malloc(numProcess * sizeof(int));
        rcounts = (int*)malloc(numProcess * sizeof(int));

        // Заполняем массивы displs, rcounts
        displs[0] = 0;
        rcounts[0] = lengthL / numProcess;
        rcounts[0] = ((lengthL - rcounts[0] * numProcess) > 0) ? rcounts[0] + 1 : rcounts[0];
        rcounts[0] *= lengthR;

        for (int i = 1; i < numProcess; i++)
        {
            displs[i] = displs[i - 1] + rcounts[i - 1];
            rcounts[i] = lengthL / numProcess;
            rcounts[i] = ((lengthL - rcounts[i] * numProcess) > i) ? rcounts[i] + 1 : rcounts[i];
            rcounts[i] *= lengthR;
        };
    };

    // Отсчёт времени работы программы
    double beginTime = MPI_Wtime();
    
    // Передаём матрицу B всем остальным процессам
    MPI_Bcast(matrixB, lengthR * lengthR, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Разделяем матрицу A на части, каждая из которых будет обрабатываться каждым процессом
    MPI_Scatterv(matrixA, rcounts, displs, MPI_DOUBLE, matrixT, rowPerProc * lengthR, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // Перемножаем матрицы T и B, записываем результат в матрицу R
    for (int i = 0; i < rowPerProc; i++)
        for (int j = 0; j < lengthR; j++)
            for (int k = 0; k < lengthR; k++)
                matrixR[lengthR * i + j] += matrixT[lengthR * i + k] * matrixB[lengthR * k + j];
    
    // Собираем со всех процессов результаты, записывая в матрицу C
    MPI_Gatherv(matrixR, rowPerProc * lengthR, MPI_DOUBLE, matrixC, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    // Конец отсчёта времени работы программы
    double endTime = MPI_Wtime();
    
    // Выводим время и значение нормы матриц A, B и C
    if (idProcess == 0)
    {
        printf("Time of running: %lf\n", endTime - beginTime);
        printf("Length of matrix: %d-%d\n", lengthL, lengthR);
        printf("Number of process: %d\n", numProcess);
        printf("Matrix A norm: %lf\n", getMatrixNorm(&matrixA, lengthL, lengthR));
        printf("Matrix B norm: %lf\n", getMatrixNorm(&matrixB, lengthR, lengthR));
        printf("Matrix C norm: %lf\n", getMatrixNorm(&matrixC, lengthL, lengthR));
    };
    
    // Очищаем память, выделенную под массивы
    if (idProcess == 0)
    {
        free(matrixA);
        free(matrixC);
        free(displs);
        free(rcounts);
    };
    free(matrixB);
    free(matrixT);
    free(matrixR);

    // Завершаем работу параллельной части программы
    MPI_Finalize();

    return 0;
}

int initializeLength(int argc, char* argv[], int* column)
{
    // Вводим через параметры программы длину матрицы
    if (argc == 2)
    {
        *column = atoi(argv[1]);
        if (*column <= 0)
        {
            printf("Error with initial parameter.\n");
            return -1;
        };
    }
    else if (argc >= 2)
    {
        printf("There are too many parameters.\n");
        return -2;
    };

    return 0;
};
void randomizeMatrixValues(double** matrix, int row, int column)
{
    for (int i = 0; i < row; i++)
        for (int j = 0; j < column; j++)
            (*matrix)[column * i + j] = (double)rand() / RAND_MAX - 0.5;
};
void printMatrix(double** matrix, int row, int column, int id)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < column; j++)
            printf("M%d[%d][%d]: %f\t", id, i, j, (*matrix)[column * i + j]);
        printf("\n");
    };

    printf("------------------------\n");
};
double getMatrixNorm(double** matrix, int row, int column)
{
    double sum = 0;
    for (int i = 0; i < row; i++)
        for (int j = 0; j < column; j++)
            sum += (*matrix)[column * i + j] * (*matrix)[column * i + j];

    return sqrt(sum);
};
