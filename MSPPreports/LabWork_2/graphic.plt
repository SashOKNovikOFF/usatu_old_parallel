reset

### For .jpeg file creation
set terminal jpeg size 1280, 1024
set output "TP-N1N2.jpg"

set xlabel "Number of process N"
set ylabel "Time"

plot "Results.txt" using ($1):($2) with lines lc 1 title 'N1 = 1000', \
     "Results.txt" using ($1):($3) with lines lc 2 title 'N2 = 1400'

### For .jpeg file creation
set terminal jpeg size 1280, 1024
set output "SP-N1N2.jpg"

set xlabel "Number of process N"
set ylabel "Speed up S"

plot "Results.txt" using ($1):(214.747967/$2) with lines lc 1 title 'N1 = 1000', \
     "Results.txt" using ($1):(622.409042/$3) with lines lc 2 title 'N2 = 1400'

### For .jpeg file creation
set terminal jpeg size 1280, 1024
set output "EP-N1N2.jpg"

set xlabel "Number of process N"
set ylabel "Efficiency E"

plot "Results.txt" using ($1):(214.747967/$2/$1) with lines lc 1 title 'N1 = 1000', \
     "Results.txt" using ($1):(622.409042/$3/$1) with lines lc 2 title 'N2 = 1400'
