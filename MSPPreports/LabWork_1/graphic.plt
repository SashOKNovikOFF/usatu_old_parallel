reset

### For .jpeg file creation
set terminal jpeg size 640, 480
set output "SP-N1N2.jpg"

set xlabel "Number of process N"
set ylabel "Speed up S"

plot "Results.txt" using ($1):(153.235948086/$2) with lines lc 1 title 'N1 = 4 * 10^9', \
     "Results.txt" using ($1):(305.720483065/$4) with lines lc 2 title 'N2 = 8 * 10^9'

### For .jpeg file creation
set terminal jpeg size 640, 480
set output "EP-N1N2.jpg"

set xlabel "Number of process N"
set ylabel "Efficiency E"

plot "Results.txt" using ($1):(153.235948086/$2/$1) with lines lc 1 title 'N1 = 4 * 10^9', \
     "Results.txt" using ($1):(305.720483065/$4/$1) with lines lc 2 title 'N2 = 8 * 10^9'
