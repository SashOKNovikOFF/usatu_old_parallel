Application statistics.
Format version: 9.1.1 Gold

Command: mpirun -mps -n 4 ./cluster 0.6 
Program: ./cluster
Number of processes: 4
------------------------------------------------------------
MPI Performance Snapshot: Statistics collection started... Writing to app_stat.txt in /home/student/SashOK/parallel/ThermalProject

==================== GENERAL STATISTICS ====================
Total time:         2222.843 sec (All ranks)

WallClock :
      MIN :          555.709 sec (rank 3)
      MAX :          555.712 sec (rank 0)

   MPI:   1.58%   NON_MPI:  98.42%

================== MEMORY USAGE STATISTICS =================
All ranks:     14.840 MB
      MIN:      3.164 MB (rank 2)
      MAX:      5.129 MB (rank 3)

================= MPI IMBALANCE STATISTICS =================
MPI Imbalance:           32.638 sec            1.468% (All ranks)
          MIN:            2.810 sec            0.506% (rank 2)
          MAX:           13.792 sec            2.482% (rank 1)
------------------------------------------------------------

WALLCLOCK in usec:
PROCESS      0:        555712097
PROCESS      1:        555711532
PROCESS      2:        555710530
PROCESS      3:        555708545

CPU Usage (times in usec):
                    PAPI_TOT_INS      PAPI_FP_INS      PAPI_VEC_FP     PAPI_LST_INS         MPI_TIME     NON_MPI_TIME
PROCESS      0:                0                0                0                0          7957805        547754294
PROCESS      1:                0                0                0                0         14537127        541174409
PROCESS      2:                0                0                0                0          3293238        552417293
PROCESS      3:                0                0                0                0          9370059        546338491

MEMORY Usage in KB:
PROCESS      0:             3352
PROCESS      1:             3352
PROCESS      2:             3240
PROCESS      3:             5252

MPI IMBALANCE in usec:
PROCESS      0:          7374800
PROCESS      1:         13792404
PROCESS      2:          2809820
PROCESS      3:          8661048

OpenMP statistics (times in usec):
                 OMP_NUM_REGIONS OMP_REGIONS_TIME OMP_IMBALANCE_TIME
PROCESS      0:                0                0                  0
PROCESS      1:                0                0                  0
PROCESS      2:                0                0                  0
PROCESS      3:                0                0                  0

