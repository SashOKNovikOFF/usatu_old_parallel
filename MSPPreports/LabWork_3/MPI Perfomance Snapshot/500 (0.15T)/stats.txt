Intel(R) MPI Library Version 5.1.1
____ MPI Communication Statistics ____

Stats level: 20
P2P scope:< FULL >
Collectives scope:< FULL >

~~~~ Process 0 of 4 on node ws09 lifetime = 137221000.91

Data Transfers
Src	Dst	Amount(MB)	Transfers
-----------------------------------------
000 --> 000	0.000000e+00	0
000 --> 001	3.925530e+01	10271
000 --> 002	0.000000e+00	0
000 --> 003	0.000000e+00	0
=========================================
Totals		3.925530e+01	10271

Communication Activity
Operation	Volume(MB)	Calls  Min time    Avr time    Max time    Total time
-----------------------------------------
P2P 
Csend           0.000000e+00	0	0.00         0.00         0.00         0.00
CSendRecv       0.000000e+00	0	0.00         0.00         0.00         0.00
Send            3.814697e-06	1	5.96         5.96         5.96         5.96
SendRecv        3.925529e+01	10270	0.95         1.32         34.09        13544.56
Bsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Rsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Ssend           0.000000e+00	0	0.00         0.00         0.00         0.00
Recv            0.000000e+00	0	0.00         0.00         0.00         0.00
Wait            0.000000e+00	0	0.00         0.00         0.00         0.00
Init            0.000000e+00	1	7601.02      7601.02      7601.02      7601.02
Finalize        0.000000e+00	1	0.00         0.00         0.00         0.00
Probe           0.000000e+00	0	0.00         0.00         0.00         0.00
Test            0.000000e+00	0	0.00         0.00         0.00         0.00
Cart            0.000000e+00	0	0.00         0.00         0.00         0.00
Buffer          0.000000e+00	0	0.00         0.00         0.00         0.00
Start           0.000000e+00	0	0.00         0.00         0.00         0.00
Request         0.000000e+00	0	0.00         0.00         0.00         0.00
Collectives
Allgather       0.000000e+00	0	0.00         0.00         0.00         0.00
Allgatherv      0.000000e+00	0	0.00         0.00         0.00         0.00
Allreduce       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoall        0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallv       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallw       0.000000e+00	0	0.00         0.00         0.00         0.00
Barrier         0.000000e+00	0	0.00         0.00         0.00         0.00
Bcast           0.000000e+00	0	0.00         0.00         0.00         0.00
Exscan          0.000000e+00	0	0.00         0.00         0.00         0.00
Gather          9.155273e-05	1	19.07        19.07        19.07        19.07
Gatherv         0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce_scatter  0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce          0.000000e+00	0	0.00         0.00         0.00         0.00
Scan            0.000000e+00	0	0.00         0.00         0.00         0.00
Scatter         0.000000e+00	0	0.00         0.00         0.00         0.00
Scatterv        0.000000e+00	0	0.00         0.00         0.00         0.00
Comm            0.000000e+00	0	0.00         0.00         0.00         0.00
=========================================

Communication Activity by actual args
P2P 
Operation    Dst    Message size      Calls    Min time    Avr time    Max time    Total time
-------------------------------------------
Send
1               1               4          1	5.96         5.96         5.96         5.96
SendRecv
1               1            4008      10270	0.95         1.32         34.09        13544.56
Collectives
Operation       Context  Algo  Comm size    Message size      Calls  Cost(%)    Min time    Avr time    Max time    Total time
---------------------------------------------------------------------------------
Gather
1                     0     3          4              96          1    0.00	19.07        19.07        19.07        19.07
=================================================================================


Collectives by Context
Context 0:  size=4  state=active  kind=intra
    #           Message size    Call count  Min time    Avr time    Max time    Total time
    Gather
    1            96               1            19.07        19.07        19.07        19.07
    Gather         total call-> 1           total time in this context--------> 19.07

Context 0: end



~~~~ Process 1 of 4 on node ws09 lifetime = 137220733.17

Data Transfers
Src	Dst	Amount(MB)	Transfers
-----------------------------------------
001 --> 000	3.925539e+01	10271
001 --> 001	0.000000e+00	0
001 --> 002	3.925530e+01	10271
001 --> 003	0.000000e+00	0
=========================================
Totals		7.851068e+01	20542

Communication Activity
Operation	Volume(MB)	Calls  Min time    Avr time    Max time    Total time
-----------------------------------------
P2P 
Csend           9.155273e-05	1	5.96         5.96         5.96         5.96
CSendRecv       0.000000e+00	0	0.00         0.00         0.00         0.00
Send            3.814697e-06	1	3.10         3.10         3.10         3.10
SendRecv        7.851059e+01	20540	0.95         95.57        49012.90     1963000.77
Bsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Rsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Ssend           0.000000e+00	0	0.00         0.00         0.00         0.00
Recv            3.814697e-06	1	4.05         4.05         4.05         4.05
Wait            0.000000e+00	0	0.00         0.00         0.00         0.00
Init            0.000000e+00	1	7604.12      7604.12      7604.12      7604.12
Finalize        0.000000e+00	1	0.95         0.95         0.95         0.95
Probe           0.000000e+00	0	0.00         0.00         0.00         0.00
Test            0.000000e+00	0	0.00         0.00         0.00         0.00
Cart            0.000000e+00	0	0.00         0.00         0.00         0.00
Buffer          0.000000e+00	0	0.00         0.00         0.00         0.00
Start           0.000000e+00	0	0.00         0.00         0.00         0.00
Request         0.000000e+00	0	0.00         0.00         0.00         0.00
Collectives
Allgather       0.000000e+00	0	0.00         0.00         0.00         0.00
Allgatherv      0.000000e+00	0	0.00         0.00         0.00         0.00
Allreduce       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoall        0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallv       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallw       0.000000e+00	0	0.00         0.00         0.00         0.00
Barrier         0.000000e+00	0	0.00         0.00         0.00         0.00
Bcast           0.000000e+00	0	0.00         0.00         0.00         0.00
Exscan          0.000000e+00	0	0.00         0.00         0.00         0.00
Gather          9.155273e-05	1	20.98        20.98        20.98        20.98
Gatherv         0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce_scatter  0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce          0.000000e+00	0	0.00         0.00         0.00         0.00
Scan            0.000000e+00	0	0.00         0.00         0.00         0.00
Scatter         0.000000e+00	0	0.00         0.00         0.00         0.00
Scatterv        0.000000e+00	0	0.00         0.00         0.00         0.00
Comm            0.000000e+00	0	0.00         0.00         0.00         0.00
=========================================

Communication Activity by actual args
P2P 
Operation    Dst    Message size      Calls    Min time    Avr time    Max time    Total time
-------------------------------------------
Csend
1               0              96          1	5.96         5.96         5.96         5.96
Send
1               2               4          1	3.10         3.10         3.10         3.10
SendRecv
1               0            4008      10270	0.95         19.98        49012.90     205210.92
2               2            4008      10270	2.86         171.16       463.96       1757789.85
Collectives
Operation       Context  Algo  Comm size    Message size      Calls  Cost(%)    Min time    Avr time    Max time    Total time
---------------------------------------------------------------------------------
Gather
1                     0     3          4              96          1    0.00	20.98        20.98        20.98        20.98
=================================================================================


Collectives by Context
Context 0:  size=4  state=active  kind=intra
    #           Message size    Call count  Min time    Avr time    Max time    Total time
    Gather
    1            96               1            20.98        20.98        20.98        20.98
    Gather         total call-> 1           total time in this context--------> 20.98

Context 0: end



~~~~ Process 2 of 4 on node ws09 lifetime = 137219618.80

Data Transfers
Src	Dst	Amount(MB)	Transfers
-----------------------------------------
002 --> 000	9.155273e-05	1
002 --> 001	3.925529e+01	10270
002 --> 002	0.000000e+00	0
002 --> 003	3.925530e+01	10271
=========================================
Totals		7.851068e+01	20542

Communication Activity
Operation	Volume(MB)	Calls  Min time    Avr time    Max time    Total time
-----------------------------------------
P2P 
Csend           9.155273e-05	1	3.10         3.10         3.10         3.10
CSendRecv       0.000000e+00	0	0.00         0.00         0.00         0.00
Send            3.814697e-06	1	5.96         5.96         5.96         5.96
SendRecv        7.851059e+01	20540	1.91         95.95        49314.98     1970780.37
Bsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Rsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Ssend           0.000000e+00	0	0.00         0.00         0.00         0.00
Recv            3.814697e-06	1	72.96        72.96        72.96        72.96
Wait            0.000000e+00	0	0.00         0.00         0.00         0.00
Init            0.000000e+00	1	6690.03      6690.03      6690.03      6690.03
Finalize        0.000000e+00	1	0.95         0.95         0.95         0.95
Probe           0.000000e+00	0	0.00         0.00         0.00         0.00
Test            0.000000e+00	0	0.00         0.00         0.00         0.00
Cart            0.000000e+00	0	0.00         0.00         0.00         0.00
Buffer          0.000000e+00	0	0.00         0.00         0.00         0.00
Start           0.000000e+00	0	0.00         0.00         0.00         0.00
Request         0.000000e+00	0	0.00         0.00         0.00         0.00
Collectives
Allgather       0.000000e+00	0	0.00         0.00         0.00         0.00
Allgatherv      0.000000e+00	0	0.00         0.00         0.00         0.00
Allreduce       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoall        0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallv       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallw       0.000000e+00	0	0.00         0.00         0.00         0.00
Barrier         0.000000e+00	0	0.00         0.00         0.00         0.00
Bcast           0.000000e+00	0	0.00         0.00         0.00         0.00
Exscan          0.000000e+00	0	0.00         0.00         0.00         0.00
Gather          9.155273e-05	1	12.16        12.16        12.16        12.16
Gatherv         0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce_scatter  0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce          0.000000e+00	0	0.00         0.00         0.00         0.00
Scan            0.000000e+00	0	0.00         0.00         0.00         0.00
Scatter         0.000000e+00	0	0.00         0.00         0.00         0.00
Scatterv        0.000000e+00	0	0.00         0.00         0.00         0.00
Comm            0.000000e+00	0	0.00         0.00         0.00         0.00
=========================================

Communication Activity by actual args
P2P 
Operation    Dst    Message size      Calls    Min time    Avr time    Max time    Total time
-------------------------------------------
Csend
1               0              96          1	3.10         3.10         3.10         3.10
Send
1               3               4          1	5.96         5.96         5.96         5.96
SendRecv
1               1            4008      10270	1.91         20.62        49314.98     211750.98
2               3            4008      10270	2.86         171.28       4027.84      1759029.39
Collectives
Operation       Context  Algo  Comm size    Message size      Calls  Cost(%)    Min time    Avr time    Max time    Total time
---------------------------------------------------------------------------------
Gather
1                     0     3          4              96          1    0.00	12.16        12.16        12.16        12.16
=================================================================================


Collectives by Context
Context 0:  size=4  state=active  kind=intra
    #           Message size    Call count  Min time    Avr time    Max time    Total time
    Gather
    1            96               1            12.16        12.16        12.16        12.16
    Gather         total call-> 1           total time in this context--------> 12.16

Context 0: end



~~~~ Process 3 of 4 on node ws09 lifetime = 137219280.96

Data Transfers
Src	Dst	Amount(MB)	Transfers
-----------------------------------------
003 --> 000	9.155273e-05	1
003 --> 001	0.000000e+00	0
003 --> 002	3.925529e+01	10270
003 --> 003	0.000000e+00	0
=========================================
Totals		3.925539e+01	10271

Communication Activity
Operation	Volume(MB)	Calls  Min time    Avr time    Max time    Total time
-----------------------------------------
P2P 
Csend           9.155273e-05	1	10.01        10.01        10.01        10.01
CSendRecv       0.000000e+00	0	0.00         0.00         0.00         0.00
Send            0.000000e+00	0	0.00         0.00         0.00         0.00
SendRecv        3.925529e+01	10270	0.95         1.12         9.06         11540.17
Bsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Rsend           0.000000e+00	0	0.00         0.00         0.00         0.00
Ssend           0.000000e+00	0	0.00         0.00         0.00         0.00
Recv            3.814697e-06	1	82.97        82.97        82.97        82.97
Wait            0.000000e+00	0	0.00         0.00         0.00         0.00
Init            0.000000e+00	1	6691.93      6691.93      6691.93      6691.93
Finalize        0.000000e+00	1	0.95         0.95         0.95         0.95
Probe           0.000000e+00	0	0.00         0.00         0.00         0.00
Test            0.000000e+00	0	0.00         0.00         0.00         0.00
Cart            0.000000e+00	0	0.00         0.00         0.00         0.00
Buffer          0.000000e+00	0	0.00         0.00         0.00         0.00
Start           0.000000e+00	0	0.00         0.00         0.00         0.00
Request         0.000000e+00	0	0.00         0.00         0.00         0.00
Collectives
Allgather       0.000000e+00	0	0.00         0.00         0.00         0.00
Allgatherv      0.000000e+00	0	0.00         0.00         0.00         0.00
Allreduce       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoall        0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallv       0.000000e+00	0	0.00         0.00         0.00         0.00
Alltoallw       0.000000e+00	0	0.00         0.00         0.00         0.00
Barrier         0.000000e+00	0	0.00         0.00         0.00         0.00
Bcast           0.000000e+00	0	0.00         0.00         0.00         0.00
Exscan          0.000000e+00	0	0.00         0.00         0.00         0.00
Gather          9.155273e-05	1	27.18        27.18        27.18        27.18
Gatherv         0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce_scatter  0.000000e+00	0	0.00         0.00         0.00         0.00
Reduce          0.000000e+00	0	0.00         0.00         0.00         0.00
Scan            0.000000e+00	0	0.00         0.00         0.00         0.00
Scatter         0.000000e+00	0	0.00         0.00         0.00         0.00
Scatterv        0.000000e+00	0	0.00         0.00         0.00         0.00
Comm            0.000000e+00	0	0.00         0.00         0.00         0.00
=========================================

Communication Activity by actual args
P2P 
Operation    Dst    Message size      Calls    Min time    Avr time    Max time    Total time
-------------------------------------------
Csend
1               0              96          1	10.01        10.01        10.01        10.01
SendRecv
1               2            4008      10270	0.95         1.12         9.06         11540.17
Collectives
Operation       Context  Algo  Comm size    Message size      Calls  Cost(%)    Min time    Avr time    Max time    Total time
---------------------------------------------------------------------------------
Gather
1                     0     3          4              96          1    0.00	27.18        27.18        27.18        27.18
=================================================================================


Collectives by Context
Context 0:  size=4  state=active  kind=intra
    #           Message size    Call count  Min time    Avr time    Max time    Total time
    Gather
    1            96               1            27.18        27.18        27.18        27.18
    Gather         total call-> 1           total time in this context--------> 27.18

Context 0: end



____ End of stats.txt file ____
